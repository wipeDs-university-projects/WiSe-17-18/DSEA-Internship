package day1.a1;

public class Fibonacci {

    public static int countOne = 0;
    public static int countFifty = 0;

    public static long[] memory;

    // O(n) thanks to lookups. Otherwise exponential.
    public static long fib (long n) {
        if (memory[(int) n] > -1) return memory[(int) n];
        if (n == 50)
            countFifty++;
        else if (n == 1) {
            countOne++;
            return 1L;
        }
        else if (n == 2)
            return 1L;

        long r = fib(n-1) + fib(n-2);
        memory[(int) n] = r;
        return r;
    }

    // O(n)
    public static long fib2 (long n) {
        long[] fibs = new long[(int) n];
        fibs[0] = 1;
        fibs[1] = 1;
        for (int i = 2; i < n; i++) {
            fibs[i] = fibs[i-1] + fibs[i-2];
        }
        return fibs[(int) n-1];
    }

    public static void main(String[] args) {
        long n = 50L;
        memory = new long[(int) n + 1];
        for (int i = 0; i <= n; i++) {
            memory[i] = -1;
        }
        System.out.println(fib(n));
        System.out.println(countOne);
        System.out.println(countFifty);
        System.out.println(fib2(n));
    }
}
