package day1.a2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Assendingpartfollows {
    //Class for assignment 2 (Aufsteigende Teil Folgen)
    public static void main(String args[]){
        ArrayList<int[]> inputs = new ArrayList<>();
        try {
            //Reads specified file and puts single numbers into an array
            FileReader fr = new FileReader("src/day1/a2/random.in");
            BufferedReader br = new BufferedReader(fr);
            int length = Integer.valueOf(br.readLine());
            for (int i = 0; i < length; i++) {
                String read = br.readLine();
                String[] readSplit = read.split(" ");
                int[] t = new int[readSplit.length-1];
                for (int j = 1; j < readSplit.length; j++) {
                    t[j-1] = Integer.valueOf(readSplit[j]);
                }
                inputs.add(t);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int[] t : inputs) {
            //System.out.print(Arrays.toString(t) + ": "); //debug
            System.out.println(count(t)); //output
        }

    }

    public static int count(int[] inputarray){
        /* helpArray stores length of maximum row up to array i. i is outer
        variable, j is inner variable. J is used to find biggest possible
        compatible row, i is used to store new biggest possible row.
        */
        int[] helpArray = new int[inputarray.length];
        int max = 0;
        for (int i = 0; i < inputarray.length; i ++){

            //initialization
            helpArray[i] = 0;
            if (i == 0)
                helpArray[0] = 1;

            for (int j = 0; j < i; j++){
                //find longest compatible preceding row
                if ((inputarray[i] > inputarray[j]) && (helpArray[i] <= helpArray[j])){
                    helpArray[i] = helpArray[j]+1;
                    if (helpArray[i] > max) max = helpArray[i];
                }
            }
        }
        //System.out.println(Arrays.toString(helpArray));
        return max;
    }
}
