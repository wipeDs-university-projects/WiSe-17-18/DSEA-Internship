package day1.a5;

import java.util.ArrayList;

public class Coin {
    /* Gegenbeispiel: Muenzen auf dem Tisch: 3 9 4 1
    1. Schritt: Ich nehme die 3 (Score 3:0) Tisch nun: 9 4 1
    2. Schritt: Gegner nimmt die 9 (Score 3:9) Tisch nun: 4 1
    3. Schritt: Ich nehme die 4 (Score 7:9) Tisch nun: 1
    4. Schritt: Gegner nimmt die 1 (Score 7:10) Tisch leer

    1. Schritt: Ich nehme die 1 (Score 1:0) Tisch : 3 9 4
    2. Schritt: Gegner nimmt die 4 (Score 1:4) Tisch: 3 9
    3. Schritt: Ich nehme die 9 (Score 10:4) Tisch 3
    4. Schritt: Gegner nimmt die 3 (Score 10:7) Tisch leer

    Man sieht, dass es hinderlich war, nur die hoechste Muenze zu spielen
     */

    public static void main (String[] args){

    }

    public static int getMoney (int[] inputList, int i, int j){
        //including i and j
        if (i == j)
            return inputList[i];

        //first case
        return -1;
    }
}
