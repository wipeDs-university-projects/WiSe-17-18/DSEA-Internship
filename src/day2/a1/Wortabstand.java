package day2.a1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Wortabstand {

    public static void main(String args[]){
        ArrayList<String[]> inputs = new ArrayList<>();
        try {
            //Reads specified file and puts single numbers into an array
            FileReader fr = new FileReader("src/day2/a1/in0.txt");
            BufferedReader br = new BufferedReader(fr);
            int length = Integer.valueOf(br.readLine());
            for (int i = 0; i < length; i++) {
                String read = br.readLine();
                String[] readSplit = read.split(" ");
                String[] t = new String[2];
                t[0] = readSplit[0];
                t[1] = readSplit[1];
                inputs.add(t);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String[] t : inputs) {
            System.out.print(Arrays.toString(t) + ": "); //debug
            System.out.println(distance(t[0], t[1])); //output
        }

    }

    public static int distance(String a, String b) {
        return distanceRecursive (a, b, a.length()-1, b.length()-1);
    }

    private static int distanceRecursive(String a, String b, int i, int j) {
        if (Math.min(i, j) == 0) {
            return Math.max(i, j);
        }
        else return Math.min(Math.min(
                distanceRecursive(a, b, i-1,j) + 1,
                distanceRecursive(a, b, i,j-1) + 1),
                distanceRecursive(a, b,i-1,j-1) + ((a.charAt(i) != b.charAt(i)) ? 1 : 0));
    }

    private static int distanceIterative (String a, String b) {
        int[][] table = new int[a.length()][b.length()];
        // Initializing first row and column to 0
        for (int i = 0; i < a.length(); i++) table[i][0] = 0;
        for (int j = 0; j < b.length(); j++) table[0][j] = 0;

        for (int i = 1; i < a.length(); i++) {
            for (int j = 1; j < b.length(); j++) {
                boolean isInBounds = (i <= a.length() - 1 && i <= b.length() - 1);
                if (isInBounds && a.charAt(i) != b.charAt(i))
                    table[i][j] = 0;
                else Math.min(Math.min(
                        table[i-1][j] + 1,
                        table[i][j-1] + 1),
                        table[i-1][j-1] + (a.charAt(i)) != b.charAt(i) ? 1 : 0);

            }
        }
        return table[a.length()-1][b.length()-1];
    }

}
