package day2.a2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Karl {

    static double[][] distances;

    private static class Node {
        int index;
        int x;
        int y;

        Node(int x, int y, int index) {
            this.x = x;
            this.y = y;
            this.index = index;
        }
        double distanceTo (Node n) {
            return distanceBetween2Planets(this.x, n.x, this.y, n.y);
        }
    }

    public static void main(String args[]) {
        try {
            //Reads specified file and puts single numbers into an array
            FileReader fr = new FileReader("src/day2/a2/sample.in");
            BufferedReader br = new BufferedReader(fr);
            int testCases = Integer.valueOf(br.readLine());
            for (int c = 0; c < testCases; c++) {
                ArrayList<Node> nodes = new ArrayList<>();
                int length = Integer.valueOf(br.readLine());
                int[] x = new int[length];
                int[] y = new int[length];
                for (int i = 0; i < length; i++) {
                    String read = br.readLine();
                    String[] readSplit = read.split(" ");
                    x[i] = Integer.valueOf(readSplit[0]);
                    y[i] = Integer.valueOf(readSplit[1]);
                    nodes.add(new Node(x[i], y[i], i));
                }
                distances = new double[x.length][y.length];
                System.out.println("Test case " + (c+1) + ": ");
                System.out.println(calculate(x, y, nodes));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static double calculate (int[] x, int[] y, ArrayList<Node> nodes) {

        // Since x are unique, first one is start, last one is "end" or the "middle one"
        Node start = nodes.get(0);
        Node end = nodes.get(nodes.size() - 1);

        // Two journeys
        ArrayList<Node> jOut  = new ArrayList<>();
        double distanceJOut = 0;
        ArrayList<Node> jBack = new ArrayList<>();
        double distanceJIn  = 0;

        jOut.add(start);
        jBack.add(end);

        for (int i = 1; i < nodes.size(); i++) {
            if (jOut.get(jOut.size()-1).distanceTo(nodes.get(i)) < 2) {
                continue;
            }
        }


        return 0;
    }

    public static double distanceBetween2Planets (int x1, int x2, int y1, int y2) {
        return Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2));
    }

}
