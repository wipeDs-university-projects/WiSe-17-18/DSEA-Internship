package day2.a3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class BigBet {

    public static int[][] helparray;

    public static void main(String args[]) {
        ArrayList<int[]> inputs = new ArrayList<>();
        try {
            //Reads specified file and puts single numbers into an array
            FileReader fr = new FileReader("src/day2/a3/sample.in");
            BufferedReader br = new BufferedReader(fr);
            int length = Integer.valueOf(br.readLine());
            for (int i = 0; i < length; i++) {
                String read = br.readLine();
                String[] readSplit = read.split(" ");
                int[] t = new int[readSplit.length-1];
                for (int j = 1; j < readSplit.length; j++) {
                    t[j-1] = Integer.valueOf(readSplit[j]);
                }
                inputs.add(t);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int[] t : inputs) {
            System.out.print(Arrays.toString(t) + ": "); //debug
            System.out.println(doTheCalculus(t)); //output
        }
    }

    // Calculates the biggest winning streak before losing again.
    public static int doTheCalculus(int[] xpctdWinnings){
        int biggestWinningStreak = 0;

        // Helparray which saves each "step" for each starting position
        // A step is just how many casinos we visit before we leave again
        // We initialize the array completely with Integer.MIN_VALUE
        // Current runtime: O(n²)
        helparray = new int[xpctdWinnings.length][xpctdWinnings.length+1];
        for (int i = 0; i < helparray.length; i++)
            for (int j = 0; j < helparray[i].length; j++)
                helparray[i][j] = Integer.MIN_VALUE;

        // Now we go through each starting position and set the initial step 0 to 0, because we didn't visit anything yet
        // This is O(n²)
        for (int startingPosition = 0; startingPosition < xpctdWinnings.length; startingPosition++) {
            helparray[startingPosition][0] = 0;
            // Now we start going around the planet, visiting more and more whilst saving the biggest amount we won
            for (int steps = 1; steps <= xpctdWinnings.length; steps++) {
                helparray[startingPosition][steps] = accumulateWinnings(xpctdWinnings, startingPosition, steps);
                if (helparray[startingPosition][steps] > biggestWinningStreak)
                    biggestWinningStreak = helparray[startingPosition][steps];
            }
        }

        // So in total, it is O(n² + n²) = O(2n²) = O(n²)

        return biggestWinningStreak;
    }

    // O(1)
    public static int accumulateWinnings(int[] xpectedWinnings, int startPos, int steps) {
        // Lookup the previous amount of money and add the "next one" onto it
        // Also, do some black voodoo magic with indices.
        int newIndex = startPos - steps;
        if (newIndex < 0) {
            newIndex = xpectedWinnings.length + newIndex;
        }
        return helparray[startPos][steps-1] + xpectedWinnings[newIndex];
    }
}
