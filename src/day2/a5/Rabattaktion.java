package day2.a5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Rabattaktion {

    public static void main(String args[]) {
        ArrayList<int[]> inputs = new ArrayList<>();
        try {
            //Reads specified file and puts single numbers into an array
            FileReader fr = new FileReader("src/day2/a5/in.txt");
            BufferedReader br = new BufferedReader(fr);
            int length = Integer.valueOf(br.readLine());
            for (int i = 0; i < length; i++) {
                String read = br.readLine();
                String[] itemsAndSeparators = read.split(" ");
                int[] t = new int[Integer.valueOf(itemsAndSeparators[0])];
                String[] items = br.readLine().split(" ");
                for (int j = 0; j < t.length; j++) {
                    t[j] = Integer.valueOf(items[j]);
                }
                System.out.println(itemsAndSeparators[1] + "|" + itemsAndSeparators[0] + ": " + Arrays.toString(t));
                System.out.println(calc(Integer.valueOf(itemsAndSeparators[1]), t, t.length - 1));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int calc (int separators, int[] items, int i) {
        if (i == 0) return 0;

        // Initialize separators
        ArrayList<ArrayList<Integer>> separatedItems = new ArrayList<>();
        for (int k = 0; k < separators; k++) {
            separatedItems.add(new ArrayList<>());
        }


        return 0;
    }

    public static int roundItems (int[] items) {
        int sum = 0;
        for (int i : items) sum += i;
        return round(sum);
    }

    public static int round (int cents) {
        double centsD = cents;
        centsD /= 10;
        centsD = Math.round(centsD);
        centsD *= 10;
        return (int) centsD;
    }

}
