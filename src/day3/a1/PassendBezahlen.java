package day3.a1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class PassendBezahlen {

    public static void main (String[] args) throws Exception {

        FileReader fr = new FileReader("src/day3/a1/in2.txt");
        BufferedReader br = new BufferedReader(fr);

        int amntTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amntTestCases; i++) {
            String[] line = br.readLine().split(" ");
            int price = Integer.valueOf(line[0]);
            int amountCoins = Integer.valueOf(line[1]);
            int[] coins = new int[amountCoins+1];
            for (int j = 1; j <= amountCoins; j++) {
                coins[j] = Integer.valueOf(line[j+1]);
            }

            System.out.println("Testcase " + (i+1));
            System.out.println(smallestAmountOfCoins(price, coins));

        }

    }

    static int smallestAmountOfCoins(int price, int[] coins) {

        System.out.println("Price: " + price + Arrays.toString(coins));

        boolean smallerCoinAvailable = false;
        for (int i: coins) {
            if (i < price && i > 0) {
                smallerCoinAvailable = true;
                break;
            }
        }

        if (!smallerCoinAvailable) return -1;

        int[][] table = new int[coins.length][price + 1];

        for (int i = 0; i < coins.length; i++) table[i][0] = 0;
        for (int j = 0; j <= price; j++) table[0][j] = 0;

        for (int i = 1; i < coins.length; i++)
            for (int j = 1; j <= price; j++) {
                if (coins[i] > j) table[i][j] = table[i-1][j];
                else {
                    int ableToGoInto = (int) Math.floor(j/coins[i]);
                    int left = ableToGoInto + table[i-1][j%coins[i]];
                    int right = table[i - 1][j];
                    table[i][j] = Math.max(left, right);
                }
                System.out.println(Arrays.deepToString(table));
            }

        return table[coins.length-1][price];
    }

}
