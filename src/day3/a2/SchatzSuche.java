package day3.a2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

public class SchatzSuche {

    public static void main (String[] args) throws Exception {

        FileReader fr = new FileReader("src/day3/a2/smaple.in");
        BufferedReader br = new BufferedReader(fr);

        int amntTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amntTestCases; i++) {
            String[] line = br.readLine().split(" ");
            int amountItems = Integer.valueOf(line[0]);
            int[] itemValues = new int[amountItems];
            for (int j = 0; j < amountItems; j++) {
                itemValues[j] = Integer.valueOf(line[j+1]);
            }

            System.out.println("Testcase " + (i+1));
            System.out.println(fairlyDividable(itemValues));

        }

    }

    static String fairlyDividable(int[] itemValues) {
        System.out.println(Arrays.toString(itemValues));
        return "YES or NO";
    }

}
