package day3.a3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

public class SchatzBergung {

    public static void main (String[] args) throws Exception {

        FileReader fr = new FileReader("src/day3/a3/in.txt");
        BufferedReader br = new BufferedReader(fr);

        int amntTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amntTestCases; i++) {
            int K = Integer.valueOf(br.readLine());
            int amountItems = Integer.valueOf(br.readLine());
            String[] strValues  = br.readLine().split(" ");
            String[] strWeights = br.readLine().split(" ");
            int[] itemValues  = new int[amountItems+1];
            int[] itemWeights = new int[amountItems+1];
            for (int j = 1; j < amountItems+1; j++) {
                itemValues[j] = Integer.valueOf(strValues[j-1]);
                itemWeights[j] = Integer.valueOf(strWeights[j-1]);
            }

            System.out.println("Testcase " + (i+1));
            System.out.println(berndHefen(K, itemValues, itemWeights));

        }

    }

    static int berndHefen(int K, int[] itemValues, int[] itemWeights) {
        int[][] table = new int [itemValues.length][K + 1];
        for (int i = 0; i < itemValues.length; i++) table[i][0] = 0;
        for (int j = 0; j < K + 1; j++) table[0][j] = 0;

        for (int i = 1; i < itemValues.length; i++)
            for (int j = 1; j <= K; j++) {
                if (itemWeights[i] > j)
                    table[i][j] = table[i-1][j];
                else {
                    int leftEntry = table[i - 1][j];
                    int rightEntry = itemValues[i] + table[i - 1][j - (itemWeights[i])];
                    table[i][j] = Math.max(leftEntry, rightEntry);
                }
            }

        return table[itemValues.length-1][K];
    }
}
