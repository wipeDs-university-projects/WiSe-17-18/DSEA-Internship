package day3.a4;

import java.io.BufferedReader;
import java.io.FileReader;

public class Piranhas {

    public static void main (String[] args) throws Exception {

        FileReader fr = new FileReader("src/day3/a4/sample.in");
        BufferedReader br = new BufferedReader(fr);

        int amntTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amntTestCases; i++) {
            String[] line = br.readLine().split(" ");
            int width = Integer.valueOf(line[0]);
            int amountBaits = Integer.valueOf(line[1]);

            //System.out.println("Testcase " + (i+1));
            System.out.println(getAmountOfRodThrows(width, amountBaits));

        }

    }

    private static int getAmountOfRodThrows(int width, int amountBaits) {

        //System.out.println(width + " - " + amountBaits);

        if (Math.ceil(Math.log((double)width)/Math.log(2.0)) < amountBaits)
            return (int) Math.ceil(Math.log((double)width)/Math.log(2.0));
        int groupBaits = amountBaits - 1;
        int groupSize = (int) Math.pow(2, groupBaits);
        return groupBaits + (int) Math.ceil(width / groupSize);

    }

}
