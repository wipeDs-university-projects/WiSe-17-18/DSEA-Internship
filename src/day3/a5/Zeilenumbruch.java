package day3.a5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

public class Zeilenumbruch {

    public static void main (String[] args) throws Exception {

        FileReader fr = new FileReader("src/day3/a5/in01.txt");
        BufferedReader br = new BufferedReader(fr);

        int amountWords = Integer.valueOf(br.readLine());
        int[] T = new int[amountWords];
        String[] strWordLengths = br.readLine().split(" ");
        for (int i = 0; i < amountWords; i++) {
            T[i] = Integer.valueOf(strWordLengths[i]);
        }

        int amntTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amntTestCases; i++) {
            int W = Integer.valueOf(br.readLine());

            System.out.println("Testcase " + (i+1));
            System.out.println(wrap(T, W));

        }

    }

    private static int wrap(int[] T, int W) {
        System.out.println(Arrays.toString(T) + "-" + W);
        return -1;
    }

}
