package day4.a1;

import java.io.BufferedReader;
import java.io.FileReader;

public class Dicesum {

    public static void main (String[] args) throws Exception {
        FileReader fr = new FileReader("src/day4/a1/sample.in");
        BufferedReader br = new BufferedReader(fr);

        int testCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < testCases; i++) {
            String[] splitty = br.readLine().split(" ");
            int amountDice = Integer.valueOf(splitty[0]);
            int diceSides = Integer.valueOf(splitty[1]);
            int eyeSum = Integer.valueOf(splitty[2]);

            System.out.println(getNumberOfWay(amountDice, diceSides, eyeSum));
        }

    }

    @SuppressWarnings("NonAsciiCharacters")
    // Doesn't work properly, i don't see why.
    private static long getNumberOfWay(int amountDice, int diceSides, int eyeSum) {
        if (amountDice * diceSides < eyeSum || amountDice > eyeSum)
            return 0;

        long[][] table = new long[amountDice + 1][eyeSum + 1];
        for (int i = 0; i <= diceSides && i <= eyeSum; i++)
            table[1][i] = 1;

        for (int i = 2; i <= amountDice; i++)
            for (int j = 1; j <= eyeSum; j++)
                for (int k = 1; k <= diceSides && k < j; k++)
                    table[i][j] = Math.addExact(table[i][j], table[i-1][j-k]) % 1000003;



        return table[amountDice][eyeSum];
    }
}
