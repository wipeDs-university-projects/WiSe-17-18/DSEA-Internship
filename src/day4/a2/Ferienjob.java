package day4.a2;

import java.io.BufferedReader;
import java.io.FileReader;

public class Ferienjob {

    static class Job {
        int start;
        int end;
        int payment;

        public Job (int start, int end, int payment) {
            this.start = start;
            this.end = end;
            this.payment = payment;
        }
    }

    public static void main (String[] args) throws Exception {
        FileReader fr = new FileReader("src/day4/a2/smaple.in");
        BufferedReader br = new BufferedReader(fr);

        int amountTestcases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amountTestcases; i++) {
            int amountJobs = Integer.valueOf(br.readLine());
            Job[] jobs = new Job[amountJobs];
            for (int j = 0; j < amountJobs; j++) {
                String[] steveJobs = br.readLine().split(" ");
                int jobStart = Integer.valueOf(steveJobs[0]);
                int jobEnd = Integer.valueOf(steveJobs[1]);
                int jobPayment = Integer.valueOf(steveJobs[2]);
                jobs[j] = new Job(jobStart, jobEnd, jobPayment);
            }
            System.out.println("Testcase " + (1+i) + ":");
            System.out.println(getTheMoney(jobs));
        }
    }

    private static int getTheMoney(Job[] jobs) {
        // max profit
        return 13371337;
    }

}
