package day4.a3;

import java.io.BufferedReader;
import java.io.FileReader;

public class MiniPalindrome {

    public static void main (String[] args) throws Exception {

        FileReader fr = new FileReader("src/day4/a3/in.txt");
        BufferedReader br = new BufferedReader(fr);

        int amountCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amountCases; i++) {
            System.out.println(isDaPalindromeQuestionMark(br.readLine()));
        }
        
    }

    private static String isDaPalindromeQuestionMark(String p) {
        boolean isPalindrome = true;
        for (int i = 0; i < Math.floor(p.length() / 2); i++)
            if (p.charAt(i) != p.charAt(p.length() - i))
                isPalindrome = false;

        if (isPalindrome) return p;
        else {
            // Do magic

            return null;
        }
    }

}
