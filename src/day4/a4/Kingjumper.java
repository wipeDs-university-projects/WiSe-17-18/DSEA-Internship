package day4.a4;

import java.io.BufferedReader;
import java.io.FileReader;

public class Kingjumper {

    public static void main (String[] args) throws Exception {

        FileReader fr = new FileReader("src/day4/a4/in.txt");
        BufferedReader br = new BufferedReader(fr);

        int amountTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amountTestCases; i++) {
            String[] schplitty = br.readLine().split(" ");
            int fieldSize = Integer.valueOf(schplitty[0]);
            int startX = Integer.valueOf(schplitty[1]);
            int startY = Integer.valueOf(schplitty[2]);
            int endX = Integer.valueOf(schplitty[3]);
            int endY = Integer.valueOf(schplitty[4]);
            int k = Integer.valueOf(schplitty[5]);

            System.out.println("Testcase " + (1+i) + ":");
            System.out.println(letsJump(fieldSize, startX, startY, endX, endY, k));
        }
        
    }

    private static long letsJump(int fieldSize, int startX, int startY, int endX, int endY, int k) {
        return Long.MAX_VALUE;
    }

}
