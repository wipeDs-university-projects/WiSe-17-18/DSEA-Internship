package day4.a5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

public class Quantumballs {

    public static void main (String[] args) throws Exception {

        FileReader fr = new FileReader("src/day4/a5/in.txt");
        BufferedReader br = new BufferedReader(fr);

        int amountTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amountTestCases; i++) {
            System.out.println("Testcase " + (1+i) + ":");
            System.out.println(areTheyReallyThereTho(br.readLine()));
        }

    }

    private static String areTheyReallyThereTho(String ball) {
        StringBuilder outputBuilder = new StringBuilder(ball);
        while (outputBuilder.charAt(0) != '.'){
            outputBuilder.deleteCharAt(0);
            outputBuilder.append('.');
        }
        //System.out.println(sb.toString());
        boolean[] availableChars = new boolean[ball.length()];
        for (int i=0; i < availableChars.length; i++)
            availableChars[i] = true;

        solveTheProblem(ball, availableChars, outputBuilder, ball);
        return outputBuilder.toString();
    }

    private static void solveTheProblem(String ball, boolean [] availableChars, StringBuilder outputString, final String startString){
        if (ball.length() == 1){
    //        System.out.println("Hello world!");
            for (int i=1; i < availableChars.length; i++){
                if (availableChars[i]){
                    outputString.replace(i,i+1, "o");
                }
            }
        }

        if ((ball.length()%2 == 0)) {
            System.out.println("Something went Windows");
            return;
        }
        //System.out.println("Into the function");

        //System.out.println("Current ball: "+ ball);
        for (int i = 1; i < ball.length()-1; i++){

          //  System.out.println("Branch number: "+i);
            boolean[] newChars = availableChars.clone();
            int happeningOccurance = 0;
            int firstOccurance = 0;
            int nextOccurance = 0;
            if (ball.charAt(i) == '<') {
            //    System.out.println("Am I here?");
                firstOccurance = i-1;
                nextOccurance = i;
            } else {
              //  System.out.println("Nope, I'm here");
                firstOccurance = i;
                nextOccurance = i+1;
            }
            for (int whatsHappening = 0; whatsHappening < startString.length(); whatsHappening++){
                if (availableChars[whatsHappening]){
                    if (happeningOccurance == firstOccurance)
                        newChars[whatsHappening] = false;
                    if (happeningOccurance == nextOccurance)
                        newChars[whatsHappening] = false;
                    happeningOccurance++;
                }
            }

            StringBuilder sb = new StringBuilder();
            for (int j=0; j<availableChars.length; j++){
                if(newChars[j]){
                    sb.append(startString.charAt(j));
                }
            }
            //System.out.println(Arrays.toString(newChars));
            //System.out.println(sb.toString() + "is now the sb.");
            solveTheProblem(sb.toString(), newChars, outputString, startString);


        }

    }

}
