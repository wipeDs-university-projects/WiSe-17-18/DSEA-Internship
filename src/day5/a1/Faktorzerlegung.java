package day5.a1;

import java.io.BufferedReader;
import java.io.FileReader;

public class Faktorzerlegung {

    public static void main (String[] args) throws Exception {
        FileReader fr = new FileReader("src/day5/a3/sample.in");
        BufferedReader br = new BufferedReader(fr);

        int amountTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amountTestCases; i++) {
            String[] splitty = br.readLine().split(" ");
            int n = Integer.valueOf(splitty[0]);
            int sizeL = Integer.valueOf(splitty[1]);
            int[] l = new int[sizeL];
            for (int j = 0; j < sizeL; j++) {
                l[j] = Integer.valueOf(splitty[j + 2]);
            }

            System.out.println("Testcase " + (i+1));
            System.out.println(factors(n, l));

        }
    }

    private static boolean factors(int n, int[] l) {
        return true;
    }

}
