package day5.a2;

import java.io.BufferedReader;
import java.io.FileReader;

public class NichtTeilfolgen {

    public static void main (String[] args) throws Exception {
        FileReader fr = new FileReader("src/day5/a2/smaple.in");
        BufferedReader br = new BufferedReader(fr);

        int amountTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amountTestCases; i++) {
            String[] splitty = br.readLine().split(" ");
            int alphabetSize = Integer.valueOf(splitty[0]);
            String s = splitty[1];


            System.out.println("Testcase " + (i+1));
            System.out.println(calculate(alphabetSize, s));

        }
    }

    private static int calculate(int alphabetSize, String string) {
        return 0;
    }

}
