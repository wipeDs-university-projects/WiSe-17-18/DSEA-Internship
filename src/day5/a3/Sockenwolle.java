package day5.a3;

import java.io.BufferedReader;
import java.io.FileReader;

public class Sockenwolle {

    public static void main (String[] args) throws Exception {
        FileReader fr = new FileReader("src/day5/a3/01-gen.in");
        BufferedReader br = new BufferedReader(fr);

        int amountTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amountTestCases; i++) {
            String s = br.readLine();

            System.out.println("Testcase " + (i+1));
            System.out.println(minimumAmountOfSprays(s));

        }
    }

    private static int minimumAmountOfSprays(String s) {
        return 0;
    }
}
