package day5.a4;

import java.io.BufferedReader;
import java.io.FileReader;

public class ErwarteteFluglaenge {

    private static class Airport {
        int x;
        int y;
        int z;

        Airport(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public static void main (String[] args) throws Exception {
        FileReader fr = new FileReader("src/day5/a4/sample.in");
        BufferedReader br = new BufferedReader(fr);

        int amountTestCases = Integer.valueOf(br.readLine());

        for (int i = 0; i < amountTestCases; i++) {

            int amountAirports = Integer.valueOf(br.readLine());

            Airport[] airports = new Airport[amountAirports];
            String[] x = br.readLine().split(" ");
            String[] y = br.readLine().split(" ");
            String[] z = br.readLine().split(" ");

            boolean[][] flights = new boolean[amountAirports][amountAirports];

            for (int k = 0; k < amountAirports; k++) {
                airports[k] = new Airport(Integer.valueOf(x[k]), Integer.valueOf(y[k]), Integer.valueOf(z[k]));
                String[] flightLine = br.readLine().split(" ");
                for (int j = 0; j < flightLine.length; j++) {
                    flights[k][j] = flightLine[j].equals("1");
                }
            }

            System.out.println("Testcase " + (i+1));
            System.out.println(expectedFlightDistance(airports, flights));
        }

    }

    private static int expectedFlightDistance(Airport[] airports, boolean[][] flights) {
        return 0;
    }

}
